"Avialable highlighting groups
"Comment        
"Constant       
"*Identifier     
"*PreProc        
"*Type   

"define highlighting groups
:highlight hiWARNING ctermfg=black cterm=bold ctermbg=gray

" This creates a keywords and puts them in the highlight groups
:syn keyword logError ERROR
:syn keyword logWarning WARNING
:syn keyword logWarn WARN
:syn keyword logInfo INFO
:syn keyword logRunAndProtect Run and protect

" nextgroup and skipwhite makes vim look for logTime after the match
:syn match logThread /^Thread\-\d\+/ nextgroup=logDate skipwhite
:syn match logThreadId /^[a-z0-9]*-[a-z0-9]*-[a-z0-9]*-[a-z0-9]*-[a-z0-9]*/ nextgroup=logDate skipwhite


" This creates a match on the date and puts in the highlight group called logDate.  The
" nextgroup and skipwhite makes vim look for logTime after the match
:syn match logDate /\d\{4}-\d\{2}-\d\{2}/ nextgroup=logTime skipwhite

" This creates a match on the time (but only if it follows the date)
:syn match logTime /\d\{2}:\d\{2}:\d\{2},\d\{3}/ nextgroup=SetupNetworks skipwhite

:syn keyword logSteupNetworks configNetwork


" Now make them appear:
" Link just links logError to the colouring for error
hi link logThread Comment
hi link logThreadId Comment
hi link logError Error
hi link logWarning Constant
hi link logWarn Constant
hi link logInfo Type
" Def means default colour - colourschemes can override
hi link logDate Conditional
hi link logTime Identifier
hi link logSteupNetworks PreProc
hi link logRunAndProtect PreProc

