execute pathogen#infect()
set tabstop=4 expandtab shiftwidth=4
set wildmenu
set incsearch
set relativenumber

filetype plugin indent on
set smartindent smarttab smartcase
set hlsearch
hi CursorColumn ctermbg=Black
hi Visual ctermbg=lightmagenta ctermfg=black

" Python
au BufRead,BufNewFile *.py set textwidth=79 colorcolumn=80 cursorcolumn list number mouse=a

" C
au BufRead,BufNewFile *.c set textwidth=79 colorcolumn=80 cursorcolumn list number mouse=a
au BufRead,BufNewFile *.h set textwidth=79 colorcolumn=80 cursorcolumn list number mouse=a

" rst files for openstack specs
au BufRead,BufNewFile *.rst set textwidth=79 colorcolumn=80 list number mouse=a

" log files escape Ansi
au BufRead,BufNewFile *.log AnsiEsc

" for yaml files
au FileType yaml setlocal ai ts=2 sw=2 et

hi! link CursorColumn CursorLine"
let ropevim_vim_completion=1
let ropevim_goto_def_newwin='vnew'
let g:ropevim_open_files_in_tabs=1

let g:pep8_map='<leader>8'

" Highlight trailing spaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
colorscheme default
